"""Vnf resources module."""
from typing import Dict, List
from uuid import uuid4

from flask_restful import Resource, request


NETWORKS = {}


class Network(Resource):
    """Network resource."""

    @staticmethod
    def reset():
        """Reset resource for tests.

        Create new, empty NETWORKS dictionary

        """
        global NETWORKS
        NETWORKS = {}

    def get(self, network_instance_id: str) -> Dict[str, List]:
        """Get network instance data.

        Get data from NETWORKS dictionary

        Args:
            network_instance_id (str): Network instance id key value

        Returns:
            Dict[str, List]: Network instance data dictionary

        """
        try:
            return NETWORKS[network_instance_id]
        except KeyError:
            NETWORKS[network_instance_id] = {
                "network-id": network_instance_id,
                "is-bound-to-vpn": False,
                "is-provider-network": False,
                "is-shared-network": False,
                "is-external-network": False,
            }
            return NETWORKS[network_instance_id]
